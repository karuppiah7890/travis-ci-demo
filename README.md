# travis-ci-demo

A demo of using [travis ci](https://travis-ci.org) with gitlab repo

So, I can't use https://travis-ci.org , I need to use https://travis-ci.com
it seems

https://docs.travis-ci.com/user/tutorial/#to-get-started-with-travis-ci-using-gitlab

For integration, it asks me to access to so many things

```

Authorize Travis CI to use your account?

An application called Travis CI is requesting access to your GitLab account. This application was created by TravisCI TravisCI. Please note that this application is not provided by GitLab and you should verify its authenticity before allowing access.

This application will be able to:
    Access the authenticated user's API
    Grants complete read/write access to the API, including all groups and projects, the container registry, and the package registry.

    Allows read-only access to the user's personal information using OpenID Connect
    Grants read-only access to the user's profile data using OpenID Connect.

    Allows read-only access to the user's primary email address using OpenID Connect
    Grants read-only access to the user's primary email address using OpenID Connect.

    Read the authenticated user's personal information
    Grants read-only access to the authenticated user's profile through the /user API endpoint, which includes username, public email, and full name. Also grants access to read-only API endpoints under /users.

    Allows read-only access to the repository
    Grants read-only access to repositories on private projects using Git-over-HTTP or the Repository Files API.

    Allows read-write access to the repository
    Grants read-write access to repositories on private projects using Git-over-HTTP (not using the API).

    Grants permission to read container registry images
    Grants read-only access to container registry images on private projects.

    Perform API actions as any user in the system
    Grants permission to perform API actions as any user in the system, when authenticated as an admin user.

    Authenticate using OpenID Connect
    Grants permission to authenticate with GitLab using OpenID Connect. Also gives read-only access to the user's profile and group memberships.
```

---

I'm writing a sample golang hello world program and then trying travis CI with
it

https://docs.travis-ci.com/user/languages/go
https://docs.travis-ci.com/user/languages/go/#default-build-script
